$(document).ready(function() {
	var disableButton = $('input[type="submit"]').prop('disabled', true);
	var enableButton = $('input[type="submit"]').prop('disabled', false);
	var txtUser = $('#user');
	var txtPass = $('#pass');
	var txtEmail = $('#email');
	var emptyUser_appended = false;
	var emptyPass_appended = false;
	var mismatch_appended = false;

	$('#mail-message').click(function() {
		$('.e-message').show(400).delay(5000).fadeOut(400);
	});

	$('input[type="submit"]').click(function() {
		if (txtUser.val() == "") {
			if (emptyUser_appended == false) {
				$('#validate ul').append('<li>username is required</li>');
				emptyUser_appended = true;
				console.log('mismatch');
			}
			return false;
		}
		else if (txtPass.val() == "") {
				if (emptyPass_appended == false) {
				$('#validate ul').append('<li>password is required</li>');
				emptyPass_appended = true;
				console.log('mismatch');
				}
			return false;
		}
		else if (txtPass.val() != $('#re-pass').val()) {
			if (mismatch_appended == false) {
				$('#validate ul').append('<li>Passwords do not match</li>');
				mismatch_appended = true;
				console.log('mismatch');
			}
			
			return false;
		}
		else{
			return true;
		}
		console.log(appended);
	});

	

	

});