from musicbox import app
from flask_mysqldb import MySQL

mysql = MySQL()
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL__PORT'] = 3306
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'C3rr3bi1sTheKey'
app.config['MYSQL_DB'] = 'musicbox_db'
mysql.init_app(app)
