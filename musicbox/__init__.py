from flask import Flask
app = Flask(__name__)

import musicbox.db_setup
import musicbox.views
import musicbox.HTTP_errors
