from musicbox import app
from flask import render_template


@app.errorhandler(404)
def not_found(error):
	title= 'Nothing here... (404)'
	description= 'Go back<a href="{{ url_for(index) }}"> home'.format(index="'index'")

	return render_template('HTTP-errors.html', title=title, description=description), 404


@app.errorhandler(403)
def not_found(error):
	title= 'NO ACCESS (403)'
	description= 'Go back<a href="{{ url_for(index) }}"> home'.format(index="'index'")

	return render_template('HTTP-errors.html', title=title, description=description), 403



@app.errorhandler(500)
def not_found(error):
	title= 'Internal Error...(505)'
	description= '<em>In the mean time:</em> <a href="http://www.omfgdogs.com/">Look! Dogs!</a>'

	return render_template('HTTP-errors.html', title=title, description=description), 500