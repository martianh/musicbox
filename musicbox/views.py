from musicbox import app
from musicbox.db_setup import mysql
from flask import render_template, redirect, url_for, request, session
from flask_uuid import FlaskUUID
import uuid


FlaskUUID(app)

@app.route('/')
def index():
	def slug(s):
		s1 = ''
		url_post = str(s)
		char = ["'",'"',"!","?","#"]
		if ' ' in url_post:
			s1 = url_post.replace(' ', '_')
		return s1

	if 'username' in session:
		user = session['username']

		connect = mysql.connection
		cursor = mysql.connection.cursor()

		cursor.execute('select e.entry_id,user_name,entry_title,cat_name,gen_name,date_format(entry_date, "%b %d, %Y") as date, unix_timestamp(sysdate())-unix_timestamp(entry_date) as time, count(co.entry_id) from Entry e inner join Category c on e.cat_id=c.cat_id inner join Genre g on e.gen_id=g.gen_id inner join User u on e.user_id=u.user_id left outer join Comment co on e.entry_id=co.entry_id group by entry_id order by entry_date desc')
		query = cursor.fetchall()
		query_length = len(query)		

		entry={
			'id' : [],
			'user' : [],
			'title' : [],
			'category': [],
			'genre' : [],
			'date' : [],
			'time' : [],
			'comment_count' : []
		}

		for i in range(len(query)):
			entry['id'].append(query[i][0])
			entry['user'].append(query[i][1])
			entry['title'].append(query[i][2])
			entry['category'].append(query[i][3])
			entry['genre'].append(query[i][4])
			entry['date'].append(query[i][5])
			com_count = query[i][7]

			if com_count == 0:
				entry['comment_count'].append('no comments')
			elif com_count >= 1:
				if com_count == 1:
					entry['comment_count'].append('1 comment')
				else:
					entry['comment_count'].append('%s comments' % com_count)


			seconds = query[i][6]
			minutes = seconds / 60
			hours = minutes / 60
			days = hours / 24
			weeks = days / 7
			months = days / 30
			years = days / 365
			#seconds
			if seconds < 60:
				if seconds <= 1:
					entry['time'].append("a second ago")
				else: 
					entry['time'].append("%s seconds ago" % seconds)
			#minutes
			elif seconds >= 60:
				if seconds == 60:
					entry['time'].append("%s minute ago" % minutes)
				elif seconds > 60 and seconds < 3600:
					entry['time'].append("%s minutes ago" % minutes)

				#hours
				elif minutes >= 60:
					if hours == 1:
						entry['time'].append("%s hour ago" % hours)
					elif hours > 1 and hours < 24:
						entry['time'].append("%s hours ago" % hours)

					#days
					elif hours >= 24:
						if days == 1:
							entry['time'].append("%s day ago" % days)
						elif days > 1 and days < 365:
							entry['time'].append("%s days ago" % days)

						#years
						elif days >= 365:
							if year == 1:
								entry['time'].append("%s year ago" % years)
							else:
								entry['time'].append("%s years ago" % years)


		return render_template('homepage.html', title='Musicbox: All about Music', user=user.capitalize(), entry=entry, query=query,query_length=query_length, slug=slug)
	else:
		return redirect(url_for('login_page'))

@app.route('/entry/<int:pid>/<post>', methods=['GET','POST'])
def post(pid, post):
	def unslug(s):
		s1 = ''
		post = str(s)
		if '_' in post:
			s1 = post.replace('_', ' ')
		return s1
	post = unslug(post)

	def unescape(text):
		txt = text
		s = ''

		if r"\r\n" in txt:
			s = txt.replace(r"\r\n", '')
			if r"\'" in s:
				s2 = s.replace(r"\'", "'")
				if '&gt;' in s2:
					s3 = s2.replace('&gt;', '>')
					if "&lt;" in s3:
						s4 = s3.replace('&lt;', '<')
						return s4
					return s3
				return s2

			elif '&gt;' in s:
				s2 = s.replace('&gt;', '>')
				if "&lt;" in s2:
					s3 = s2.replace('&lt;', '<')
					return s3
				return s2
			return s

		elif '&gt;' in txt:
			s = txt.replace('&gt;', '>')
			if "&lt;" in s:
				s2 = s.replace('&lt;', '<')
				return s2
			return s

		else:
			return txt

	user = ''
	if 'username' in session:
		user = session['username']
		if request.method == 'POST':
			text = request.form['text']
			username = session['username']
			connect = mysql.connection
			cursor = mysql.connection.cursor()

			sql='insert into Comment(com_parentid,com_text,com_date,user_id,entry_id) values(null, %s, sysdate(), (select user_id from User where user_name=%s), %s)'
			cursor.execute(sql, (text, username, pid))
			connect.commit()


		connect = mysql.connection
		cursor = mysql.connection.cursor()
		date = r"%b %d, %Y"

		sql = 'select entry_text,date_format(entry_date, %s) from Entry where entry_id=%s'
		cursor.execute(sql, (date, pid))
		query = cursor.fetchone()
		text = query

		sql = 'select user_name, com_text from Comment c inner join User u on c.user_id=u.user_id where entry_id=%s'
		cursor.execute(sql, [pid])
		query = cursor.fetchall()
		
		comment = query
		com_length = len(query)

		return render_template('entry.html', title=post, post=post, text=text, comment=comment, com_length=com_length, unescape=unescape, user=user.capitalize())
	else:
		return redirect(url_for('login_page'))


@app.route('/entry')
def entry():
	return render_template('entry.html', title="Test post - MusicBox")

	
@app.route('/login', methods=['GET', 'POST'])
def login_page():
	log = ""
	if request.method == 'POST':
		connect = mysql.connection
		cursor = mysql.connection.cursor()

		username = request.form['username'].encode('utf-8')
		password = request.form['password'].encode('utf-8')
		sql = 'select * from User where user_name=%s and  user_passwd=%s'
		cursor.execute(sql,(username.lower(), password))
		query = cursor.fetchone()
		if query is None:
			log = "Wrong Username/Password."
		else:
			session["username"] = username.lower()
			return redirect(url_for('index'))
	return render_template('login.html', title="Log-in", log=log, session=session)


@app.route('/logout')
def logout():
	session.pop('username', None)
	return redirect(url_for('index'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
	if request.method == 'POST':
		connect = mysql.connection
		cursor = mysql.connection.cursor()

		new_user = request.form['new_username'].encode('utf-8')
		new_passwd = request.form['new_password'].encode('utf-8')
		new_email = request.form['new_email'].encode('utf-8')

		if new_email == "":
			cursor.execute('insert into User(user_name, user_email, user_passwd) values(%s, null, %s)', (new_user.lower(), new_passwd))
			
		else:
			sql='insert into User(user_name, user_email, user_passwd) values(%s, %s, %s)'
			cursor.execute(sql, (new_user.lower(), new_email.lower(), new_passwd))
		connect.commit()
	return render_template('signup.html', title="Create Account")


@app.route('/submit_entry', methods=['GET', 'POST'])
def submit():
	if 'username' in session:
		user = session['username']
		if request.method == 'POST':
			connect = mysql.connection
			cursor = mysql.connection.cursor()

			title = request.form['title']
			text = request.form['content']
			user= session['username']
			category = request.form['category']
			genre= request.form['genre']

			sql='insert into Entry(entry_date,entry_title,entry_text,user_id,cat_id,gen_id) values(sysdate(),%s, %s,(select user_id from User where user_name = %s),(select cat_id from Category where cat_name = %s), (select gen_id from Genre where gen_name = %s))'

			cursor.execute(sql, (title, text, user, category, genre))
			connect.commit()

			return redirect(url_for('index'))
			
		connect = mysql.connection
		cursor = mysql.connection.cursor()
		cursor.execute('select cat_name from Category')
		query = cursor.fetchall();
		category = []

		for i in query:
			category.append(str(i)[3:len(i)-4])

		cat_length = len(category)

		cursor.execute('select gen_name from Genre')
		query = cursor.fetchall();
		genre = []

		for i in query:
			genre.append(str(i)[3:len(i)-4])

		gen_length = len(category)


		return render_template('submit.html', title="Preach to the choire!", category=category, cat_length=cat_length, genre=genre, gen_length=gen_length, user=user)
	else:
		return redirect(url_for('login_page'))


@app.route('/user/<username>')
def user(username):
	return "Welcome {0}".format(username)


@app.route('/test')
def test():
	u = uuid.uuid4()
	return redirect(url_for('uid', id=u))


@app.route('/test/<uuid(strict=False):id>')
def uid(id):
	return repr(id)
	

app.secret_key = '345b2772383613d5986bd6d7c6a3dc34bd0bc0bfd3097d9e'
